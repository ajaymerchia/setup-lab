#!/bin/bash
CONFIG=~/config.txt

touch $CONFIG
echo "Hello!"
echo ""
echo "This is a quick script to initialize a config file that can make starting labs much faster for you."
echo "IT IS EXTREMELY IMPORTANT THAT YOU DO NOT USE APOSTROPHES OR QUOTATION MARKS. ENTER EVERYTHING IN NORMALLY."

echo "What is your instructional account login? (e.g. abc)"
read THREELOGx

echo "What is the location of your lab files? For example, if you stored lab5 in a directory ~/labs/05, enter ~/labs/. Don't forget the trailing backslash."
read LABLOCx

echo "What is your bitbucket username? (e.g. nickweaver)"
read BBUSRx

echo "What is your bitbucket password (e.g. password123)"
read BBPWx

echo ""
echo ""

echo "Creating config file..."

echo "#!/bin/bash" >> $CONFIG
echo "set -e" >> $CONFIG
echo "THREELOG=$THREELOGx" >> $CONFIG
echo "LABLOC=$LABLOCx" >> $CONFIG
echo "BBUSR=$BBUSRx" >> $CONFIG
echo "BBPW=$BBPWx" >> $CONFIG

echo "Making bin..."
mkdir ~/.bin/

echo "Adding bin to bash profile..."
echo "export PATH=$PATH:~/.bin" >> ~/.bash_profile
echo "Exported Path, executing bash"
source ~/.bash_profile

echo "Configuring labstart script..."
cat labstart >> ~/config.txt

echo "Making executable..."
mv ~/config.txt ~/.bin/labstart
chmod +x ~/.bin/labstart

echo "Clearing up files..."
rm -rf labstart

echo "Done! Whenever you start a lab, simply enter labstart ## (e.g. labstart 01). "
